from woocommerce import API
import json
import logging
logger = logging.getLogger()


class WooCommerce(object):
    def __init__(self, integration):
        self.consumer_key = ''
        self.consumer_secret = ''
        self.url = ''
        self.wcapi = None
        self.activate(integration)

    def activate(self, integration):
        if integration:
            self.url = integration['baseUrl']
            self.consumer_key = integration['accessToken']
            self.consumer_secret = integration['refreshToken']
            self.wcapi = API(
                url=self.url,
                consumer_key=self.consumer_key,
                consumer_secret=self.consumer_secret,
                wp_api=True,
                version="wc/v2",
                timeout=30
            )

    ########################################
    ##########  WooCommerce Api  ###########
    ########################################
    def search_product_by_sku(self, sku):
        product = None
        endpoint = 'products?sku=' + sku
        if sku:
            response = self.wcapi.get(endpoint)
            if response and response.status_code == 200:
                products = json.loads(response.content)
                if products and len(products) > 0:
                    product = products[0]
        return product

    def create_product(self, product):
        endpoint = 'products'
        if self.wcapi:
            response = self.wcapi.post(endpoint, product)
            if response:
                print(json.loads(response.content))

    def update_product(self, product_id, product):
        endpoint = 'products/' + str(product_id)
        if self.wcapi:
            self.wcapi.put(endpoint, product)

    def search_category_by_slug(self, slug):
        category = None
        endpoint = 'products/categories?slug=' + slug
        if slug:
            response = self.wcapi.get(endpoint)
            if response and response.status_code == 200:
                categories = json.loads(response.content)
                if categories and len(categories) > 0:
                    category = categories[0]
        return category

    def create_category(self, category):
        endpoint = 'products/categories'
        if self.wcapi:
            category = self.wcapi.post(endpoint, category).json()
        return category

    def update_category(self, category_id, category):
        endpoint = 'products/categories/' + str(category_id)
        if self.wcapi:
            self.wcapi.put(endpoint, category)

    ########################################
    ### Model Adapter & Helper Function  ###
    ########################################

    @staticmethod
    def parse_item_images(item):
        images = []
        if item and item['assets']:
            for index, asset in enumerate(item['assets']):
                if 'large_url' in asset:
                    image = {
                        'src': asset['large_url'],
                        'position': index
                    }
                    images.append(image)
        return images

    def parse_item_categories(self, item_category_value):
        """
        :param item_category_value: Hardware > Knobs > pulls > Knifes
        :return: 
        """
        categories = []
        if item_category_value:
            category_list = item_category_value.split(' > ')
            for name in category_list:
                category = self.search_category_by_slug(name.lower())
                if category:
                    categories.append(category)
                else:
                    category = {
                        'name': name,
                        'slug': name.lower()
                    }
                    if len(categories) > 0 and 'id' in categories[-1]:
                        category['parent'] = categories[-1]['id']

                    category = self.create_category(category)
                    if 'id' in category:
                        categories.append(category)
        return categories
