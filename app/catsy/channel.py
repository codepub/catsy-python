import json
import requests
from .mixins import Header
from .session import API
import logging
logger = logging.getLogger()


class Channel(Header):
    def __init__(self, token=None):
        self.token = token

    def get(self, channel_id):
        """
        Get Item
        :param channel_id: 
        :return: channel
        """
        channel = None
        url = API.API_V3_HOST + 'channels/' + channel_id
        try:
            response = requests.get(url, headers=self.header(self.token))
            if response.status_code == 200:
                content = json.loads(response.content)
                channel = content['channel']
        except requests.ConnectionError:
            logger.error('Request Connection Error.')
        return channel

    def items(self, channel_id, limit=10, offset=0, action=None, begin_date=None, end_date=None):
        """
        :param channel_id: 
        :param limit: 
        :param offset:
        :param action: 
        :param begin_date:
        :param end_date:
        :return: 
        """
        url = API.API_V3_HOST + 'channels/' + channel_id + '/items'
        params = {
            'limit': limit,
            'offset': offset,
            'action': action,
            'beginDate': begin_date,
            'endDate': end_date
        }
        items = []
        try:
            response = requests.get(url, headers=self.header(self.token), params=params)
            if response.status_code == 200:
                content = json.loads(response.content)
                items = content['items']
        except requests.ConnectionError:
            logger.error('Request Connection Error.')
        return items
