import json
import requests
import logging
import time
from .mixins import Header
from .session import API
logger = logging.getLogger()


class Item(Header):
    """
    Api Service of Item
    """

    def __init__(self, token=None):
        self.token = token

    def get(self, item_id):
        """
        Get Item
        :param item_id: 
        :return: item
        """
        item = None
        url = API.API_V3_HOST + 'items/' + str(item_id)
        params = {}
        try:
            response = requests.get(url, headers=self.header(self.token), params=params)
            if response.status_code == 200:
                content = json.loads(response.content)
                item = content['item']
        except requests.ConnectionError:
            logger.error('Request Connection Error.')
        return item

    def update_value(self, item_id, key, value):
        """
        Update Item Attribute Value
        :param item_id: item id
        :param key: attribute key
        :param value: attribute value
        :return: saved
        """
        saved = False
        url = API.API_V3_HOST + 'items/' + str(item_id) + '/values'
        body = {
            key: value
        }
        params = {}
        try:
            response = requests.put(url, headers=self.header(self.token), data=json.dumps(body), params=params)
            if response.status_code == 200:
                saved = True
        except requests.ConnectionError:
            logger.error('Request Connection Error.')
        return saved

    def delete(self, item_id):
        """
        Delete Item
        :param item_id: item id
        :return: deleted
        """
        deleted = False
        url = API.API_V3_HOST + 'items'
        try:
            response = requests.delete(url, headers=self.header(self.token), data=str(item_id))
            if response.status_code == 200:
                deleted = True
        except requests.ConnectionError:
            logger.error('Request Connection Error.')
        return deleted

    def items(self, locale='', limit=50, offset=0):
        """
        Get items
        :param locale: 
        :param limit: 
        :param offset: 
        :return: 
        """
        url = API.API_V3_HOST + 'items'
        params = {
            'locale': locale,
            'limit': limit,
            'offset': offset
        }
        items = []
        try:
            response = requests.get(url, headers=self.header(self.token), params=params)
            if response.status_code == 200:
                content = json.loads(response.content)
                items = content['items']
        except requests.ConnectionError:
            logger.error('Request Connection Error.')
        return items

    def get_items(self, filters, attribute_keys, limit, offset, retry=5):
        """
        :param filters: 
        :param limit: 
        :param offset:
        :param locale: 
        :return: 
        """
        url = API.API_V3_HOST + 'items/filters'
        params = {
            'limit': limit,
            'offset': offset,
        }
        data = {
            'filters': filters,
            'attributeKeys': attribute_keys
        }
        items = []
        try:
            response = requests.post(url, headers=self.header(self.token), params=params, data=json.dumps(data))
            if response.status_code == 200:
                content = json.loads(response.content)
                items = content['items']
            else:
                raise Exception(response.content)
        except:
            if retry > 0:
                time.sleep(3)
                items = self.get_items(filters, attribute_keys, limit, offset, retry - 1)
            else:
                raise Exception(error)
        return items
