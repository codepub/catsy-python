import json
import requests
import logging
from .mixins import Header
from .session import API
logger = logging.getLogger()


class Integration(Header):
    """
    Api Service of Item
    """

    def __init__(self, token=None):
        self.token = token

    def get_key(self, integration, version='1', used_by='CATSY'):
        """
        Get Item
        :param integration: 
        :param version: 
        :param used_by:  
        :return: integration key
        """
        key = None
        url = API.API_V3_HOST + 'integration/key'
        params = {
            'integration': integration,
            'version': version,
            'usedBy': used_by
        }
        try:
            response = requests.get(url, headers=self.header(self.token), params=params)
            if response.status_code == 200:
                content = json.loads(response.content)
                key_list = content['key']
                if type(key_list) is list:
                    key = key_list[0]
        except requests.ConnectionError:
            logger.error('Request Connection Error.')
        return key

    def update_key(self, integration):
        """
        Update Item Attribute Value
        :param integration: integration object
        :return: saved
        """
        saved = False
        url = API.API_V3_HOST + 'integration/key'
        try:
            response = requests.post(url, headers=self.header(self.token), data=json.dumps(integration))
            if response.status_code == 200:
                saved = True
        except requests.ConnectionError:
            logger.error('Request Connection Error.')
        return saved

