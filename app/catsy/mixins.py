class Header(object):

    @staticmethod
    def header(token, content_type='application/json'):
        return {
            'Authorization': 'Bearer ' + token,
            'content-type': content_type
        }

    @staticmethod
    def auth(token):
        return {
            'Authorization': 'Bearer ' + token
        }
