import json
import requests
from .mixins import Header
from .session import API
import logging
logger = logging.getLogger()


class Collection(Header):
    def __init__(self, token=None):
        self.token = token

    def items(self, collection_id, limit=10, offset=0, action=None, begin_date=None, end_date=None):
        """
        :param collection_id: 
        :param limit: 
        :param offset:
        :param action: 
        :param begin_date:
        :param end_date:
        :return: 
        """
        url = API.API_V3_HOST + 'collections/' + str(collection_id) + '/items'
        params = {
            'limit': limit,
            'offset': offset,
            'action': action,
            'beginDate': begin_date,
            'endDate': end_date
        }
        items = []
        try:
            response = requests.get(url, headers=self.header(self.token), params=params)
            if response.status_code == 200:
                content = json.loads(response.content)
                items = content['items']
        except requests.ConnectionError:
            logger.error('Request Connection Error.')
        return items


    def addItem(self, collection_id, skus):
        url = API.API_V3_HOST + 'collections/' + str(collection_id) + '/items'
        data = {
            'skus': skus
        }
        try:
            requests.post(url, headers=self.header(self.token), data=json.dumps(data))
        except requests.ConnectionError:
            logger.error('Request Connection Error.')


    def removeItem(self, collection_id, skus):
        url = API.API_V3_HOST + 'collections/' + str(collection_id) + '/items'
        data = {
            'skus': skus
        }
        try:
            requests.delete(url, headers=self.header(self.token), data=json.dumps(data))
        except requests.ConnectionError:
            logger.error('Request Connection Error.')