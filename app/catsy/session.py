class API(object):
    BASE_API_URL = 'https://api.catsy.com/'
    API_VERSION = 'api/v3/'

    API_V3_HOST = BASE_API_URL + API_VERSION
