from .attribute import Attribute
from .item import Item
from .asset import Asset
from .integration import Integration
from .collection import Collection
from .channel import Channel
from .query import Query


class Catsy(object):

    def __init__(self, token=None):
        self.token = token

    def attribute(self):
        """
        Initialize a :class:`Attribute` object
        :return: 
        """
        return Attribute(self.token)

    def collection(self):
        """
        Initialize a :class:`Collection` object
        :return: 
        """
        return Collection(self.token)

    def item(self):
        """
        Initialize a :class:`Item` object
        :return: 
        """
        return Item(self.token)

    def asset(self):
        """
        Initialize a :class:`Asset` object
        :return: 
        """
        return Asset(self.token)

    def channel(self):
        """
        Initialize a :class:`Channel` object
        :return: 
        """
        return Channel(self.token)

    def integration(self):
        """
        Initialize a :class:`Integration` object
        :return: 
        """
        return Integration(self.token)

    def query(self):
        """
        Initialize a :class:`Integration` object
        :return:
        """
        return Query(self.token)
