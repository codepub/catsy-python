import json
import requests
import time
from .mixins import Header
from .session import API
import logging

logger = logging.getLogger()


class Attribute(Header):
    def __init__(self, token=None):
        self.token = token

    def get_attributes(self, data_type=''):
        """
        Get Catsy Attributes
        :param data_type: 
        :return: 
        """
        attributes = []
        url = API.API_V3_HOST + 'attributes'
        params = {
            'dataType': data_type
        }
        try:
            response = requests.get(url, headers=self.header(self.token), params=params)
            if response.status_code == 200:
                content = json.loads(response.content)
                attributes = content['attributes']
            else:
                raise Exception(response.content)
        except requests.ConnectionError:
            raise Exception('Request Connection Error.')
        return attributes

    def get_attribute_map(self, data_type='', retry=10):
        attribute_map = {}
        try:
            attributes = self.get_attributes(data_type)
            for attribute in attributes:
                if 'key' in attribute and attribute['key']:
                    attribute_map[attribute['key']] = attribute
        except Exception as error:
            if retry > 0:
                time.sleep(3)
                attribute_map = self.get_attribute_map(data_type, retry - 1)
            else:
                raise Exception(error)

        return attribute_map
