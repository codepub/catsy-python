import json
import requests
import logging
from .mixins import Header
from .session import API
logger = logging.getLogger()


class Asset(Header):
    """
    Api Service of Asset
    """

    def __init__(self, token=None):
        self.token = token

    def upload(self, asset_path):
        """
        Create Asset
        :param asset_path: 
        :return: 
        """
        asset = None
        url = API.API_V3_HOST + 'assets'
        files = {
            'imageData': open(asset_path, 'rb')
        }
        try:
            response = requests.post(url, headers=self.auth(self.token), files=files)
            if response.status_code == 200:
                content = json.loads(response.content)
                asset = content['asset']
            else:
                print(response.content)
        except requests.ConnectionError:
            logger.error('Request Connection Error.')
        return asset

    def list(self, limit=50, offset=0):
        """
        Get Item
        :param limit: 
        :param offset: 
        :return: assets
        """
        assets = []
        url = API.API_V3_HOST + 'assets'
        params = {
            'limit': limit,
            'offset': offset
        }
        try:
            response = requests.get(url, headers=self.header(self.token), params=params)
            if response.status_code == 200:
                content = json.loads(response.content)
                assets = content['assets']
        except requests.ConnectionError:
            logger.error('Request Connection Error.')
        return assets
