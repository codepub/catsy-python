import json
import requests
import logging
import time
from .mixins import Header
from .session import API

logger = logging.getLogger()


class Query(Header):
    def __init__(self, token=None):
        self.token = token

    def get(self, query_id, retry=10):
        url = API.API_V3_HOST + 'queries/' + str(query_id)
        query = None
        try:
            response = requests.get(url, headers=self.header(self.token))
            if response.status_code == 200:
                content = json.loads(response.content)
                query = content['query']
            else:
                raise Exception(response.content)
        except Exception as error:
            if retry > 0:
                time.sleep(3)
                query = self.get(query_id, retry - 1)
            else:
                raise Exception(error)
        return query

    def items(self, query_id, limit=10, offset=0, retry=10):
        url = API.API_V3_HOST + 'queries/' + str(query_id) + '/items'
        params = {
            'limit': limit,
            'offset': offset
        }
        items = []
        try:
            response = requests.get(url, headers=self.header(self.token), params=params)
            if response.status_code == 200:
                content = json.loads(response.content)
                items = content['items']
            else:
                raise Exception(response.content)
        except Exception as error:
            if retry > 0:
                time.sleep(3)
                items = self.items(query_id, limit, offset, retry - 1)
            else:
                raise Exception(error)
        return items

    def items_count(self, query_id, retry=10):
        url = API.API_V3_HOST + 'queries/' + str(query_id) + '/items/count'
        count = None
        try:
            response = requests.get(url, headers=self.header(self.token))
            if response.status_code == 200:
                content = json.loads(response.content)
                count = content['count']
            else:
                raise Exception(response.content)
        except Exception as error:
            if retry > 0:
                time.sleep(3)
                count = self.items_count(query_id, retry - 1)
            else:
                raise Exception(error)
        return count

