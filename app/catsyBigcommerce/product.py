class Product(object):
    default_price = 9999.99

    def __init__(self):
        self.name = u'NA'
        self.sku = u'NA'
        self.price = self.default_price
        # TODO: root category 0 is not available, need fix here
        self.categories = [23]
        self.type = u'physical'
        self.weight = 0
        self.description = u''
        self.is_free_shipping = True

    def set_name(self, name):
        self.name = name

    def get_name(self):
        return self.name

    def set_sku(self, sku):
        self.sku = sku

    def get_sku(self):
        return self.sku

    def set_price(self, price):
        self.price = price

    def get_price(self):
        return self.price

    def set_categories(self, categories):
        self.categories = categories

    def get_categories(self):
        return self.categories

    def set_type(self, type):
        self.type = type

    def get_type(self):
        return self.type

    def set_weight(self, weight):
        self.weight = weight

    def get_weight(self):
        return self.weight

    def set_description(self, description):
        self.description = description

    def get_description(self):
        return self.description

    def set_free_shipping(self, free):
        self.is_free_shipping = free

    def get_free_shipping(self):
        return self.is_free_shipping
