import json, requests
import logging
import os, re


class Bigcommerce(object):
    logger = logging.getLogger('syncProduct')

    def __init__(self, client_id, client_token, store_url):
        self.token = client_token
        self.client = client_id
        self.api_url = store_url
        self.temp_file_name = u'tmp.jpg'
        self.bigcommerce_header = {
            u'X-Auth-Token': self.token,
            u'X-Auth-Client': self.client,
            u'Accept': u'application/json'
        }
        # Cache brands and categories
        self.brands = self.get_brand()
        self.categories = self.get_category()
        # self.get_category_tree()

    def get_brand(self, brand_name=None):
        """
        Get brand
        :param brand_name:
        :return:
        """
        # Get
        brand = []
        get_brand_url = self.api_url + u'v2/brands'
        params = {u'name': brand_name, u'limit': 50} if brand_name else {u'limit': 50}
        try:
            response = requests.get(get_brand_url, headers=self.bigcommerce_header, params=params)
            if response.status_code == 200:
                brand = json.loads(response.content)
            else:
                self.logger.warning("Error get brand info : {0}".format(response.content))
        except requests.ConnectionError:
            self.logger.error('Get Brand Request Error.')
        except KeyError:
            self.logger.error('Get Brand Key Error.')
        return brand

    def create_brand(self, brand):
        """
        Create brand
        :param brand:
        :return:
        brand = {
            u'name' = brand_name
        }
        """
        data = None
        category = json.dumps(brand)
        create_brand_url = self.api_url + u'v3/catalog/brands'
        try:
            response = requests.post(create_brand_url, headers=self.bigcommerce_header, data=category)
            if response.status_code == 200:
                content = json.loads(response.content)
                data = content[u'data']
                # If create new brand successfully, update self.brands cache
                self.brands = self.get_brand()
            else:
                self.logger.warning("Error create Brand info : {0}".format(response.content))
        except requests.ConnectionError:
            self.logger.error('Create Brand Request Error.')
        except KeyError:
            self.logger.error('Create Brand Key Error.')
        return data

    def get_product(self, sku):
        """
        Get product by sku
        :param sku:
        :return:
        """
        # Get
        data = []
        get_product_url = self.api_url + u'v2/products'
        params = {u'sku': sku} if sku else {}
        try:
            response = requests.get(get_product_url, headers=self.bigcommerce_header, params=params)
            if response.status_code == 200:
                data = json.loads(response.content)
            else:
                self.logger.warning("Error get product info : {0}".format(response.content))
        except requests.ConnectionError:
            self.logger.error('Get Product Request Error.')
        except KeyError:
            self.logger.error('Get Product Key Error.')
        return data

    def get_product_by_name(self, name):
        """
        Get product by name
        :param name:
        :return:
        """
        # Get
        data = []
        get_product_url = self.api_url + u'v2/products'
        params = {u'name': name} if name else {}
        try:
            response = requests.get(get_product_url, headers=self.bigcommerce_header, params=params)
            if response.status_code == 200:
                data = json.loads(response.content)
            else:
                self.logger.warning("Error get product info : {0}".format(response.content))
        except requests.ConnectionError:
            self.logger.error('Get Product By Name Request Error.')
        except KeyError:
            self.logger.error('Get Product By Name Key Error.')
        return data

    def create_product(self, product):
        """
        Create Product On BigCommerce
        :param product: productObj
        :return:
        """
        # Post
        data = None
        product_json = json.dumps(product.__dict__)
        create_product_url = self.api_url + u'v3/catalog/products'
        try:
            response = requests.post(create_product_url, headers=self.bigcommerce_header, data=product_json)
            if response.status_code == 200:
                content = json.loads(response.content)
                data = content[u'data']
                # self.logger.warning("Create product info : {0}".format(data))
            elif response.status_code == 409:
                data = {u'status': 409}
                # Get product sku, if the sku is different, then modify the product name and create again
                # If sku are the same, then they are the same items, skip
                duplicate_product = self.get_product_by_name(product.get_name())
                if len(duplicate_product) > 0 and duplicate_product[0][u'sku'] and duplicate_product[0][u'sku'] != product.get_sku():
                    duplicate_product_name = product.get_name() + u'--' + product.get_sku()
                    product.set_name(duplicate_product_name)
                    data = self.create_product(product)
                if not data or data.has_key(u'status'):
                    self.logger.warning("Error create product info : {0}".format(data))
            else:
                self.logger.warning("Error create product info : {0}".format(response.content))
        except requests.ConnectionError:
            self.logger.info('Create Product Request Error.')
        except KeyError:
            self.logger.info('Create Product Key Error.')
        return data

    def update_product(self, product_id, product):
        """
        Update Product On BigCommerce
        :param product_id:
        :param product: productObj
        :return:
        """
        # Put
        data = None
        product_json = json.dumps(product.__dict__)
        update_product_url = self.api_url + u'v3/catalog/products/' + str(product_id)
        try:
            response = requests.put(update_product_url, headers=self.bigcommerce_header, data=product_json)
            if response.status_code == 200:
                content = json.loads(response.content)
                data = content[u'data']
                # self.logger.warning("Update product info : {0}".format(data))
            elif response.status_code == 409:
                data = {u'status': 409}
                # Get product name, if the name is different, then keep the original name
                duplicate_product = self.get_product(product.get_sku())
                if len(duplicate_product) > 0 and duplicate_product[0][u'name'] and duplicate_product[0][u'name'] != product.get_name():
                    duplicate_product_name = duplicate_product[0][u'name']
                    product.set_name(duplicate_product_name)
                    data = self.update_product(product_id, product)
                if not data or data.has_key(u'status'):
                    self.logger.warning("Error update product info : {0}".format(data))
            else:
                self.logger.warning("Error update product info : {0}".format(response.content))
        except requests.ConnectionError:
            self.logger.error('Update Product Request Error.')
        except KeyError:
            self.logger.error('Update Product Key Error.')
        return data

    def get_category(self, parent_id=None, category_name=None):
        """
        Get Category by name
        :param parent_id
        :param category_name:
        :return:
        """
        # Get
        category = []
        get_category_url = self.api_url + u'v2/categories'
        params = {u'name': category_name, u'limit': 50} if category_name else {u'limit': 50}
        if parent_id:
            params[u'parent_id'] = parent_id
        try:
            response = requests.get(get_category_url, headers=self.bigcommerce_header, params=params)
            if response.status_code == 200:
                category = json.loads(response.content)
                # self.logger.warning("Get category info : {0}".format(category))
                # category = content[u'data']
            else:
                self.logger.warning("Error get category info : {0}".format(response.content))
        except requests.ConnectionError:
            self.logger.error('Get Category Request Error.')
        except KeyError:
            self.logger.error('Get Category Key Error.')
        return category

    def get_category_by_id(self, category_id):
        """
        Get Category by id
        :param category_id
        :return:
        """
        # Get
        category = []
        get_category_url = self.api_url + u'v2/catalog/categories/' + str(category_id)

        try:
            response = requests.get(get_category_url, headers=self.bigcommerce_header)
            if response.status_code == 200:
                category = json.loads(response.content)
                # self.logger.warning("Get category info : {0}".format(category))
                # category = content[u'data']
            else:
                self.logger.warning("Error get category info : {0}".format(response.content))
        except requests.ConnectionError:
            self.logger.error('Get Category Request Error.')
        except KeyError:
            self.logger.error('Get Category Key Error.')
        return category

    def get_category_tree(self):
        """
        Get Category tree
        :return:
        """
        # Get
        category = []
        get_category_url = self.api_url + u'v3/catalog/categories/tree'

        try:
            response = requests.get(get_category_url, headers=self.bigcommerce_header)
            if response.status_code == 200:
                category = json.loads(response.content)
                # self.logger.warning("Get category info : {0}".format(category))
                # category = content[u'data']
            else:
                self.logger.warning("Error get category info : {0}".format(response.content))
        except requests.ConnectionError:
            self.logger.error('Get Category Request Error.')
        except KeyError:
            self.logger.error('Get Category Key Error.')
        return category

    def create_category(self, category):
        """
        Create Category
        :param category:
        :return:
        category = {
            u'parent_id': 0,
            u'name': master_category_name
        }
        """
        # Post
        data = None
        category = json.dumps(category)
        # self.logger.warning("Category info : {0}".format(category))
        create_category_url = self.api_url + u'v3/catalog/categories'
        try:
            response = requests.post(create_category_url, headers=self.bigcommerce_header, data=category)
            if response.status_code == 200:
                content = json.loads(response.content)
                data = content[u'data']
                # If create new category successfully, update self.categories cache
                self.categories = self.get_category()
            else:
                self.logger.warning("Error create category info : {0}".format(response.content))
        except requests.ConnectionError:
            self.logger.error('Create Category Request Error.')
        except KeyError:
            self.logger.error('Create Category Key Error.')
        except:
            pass
        return data

    def get_image(self, product_id):
        """
        Upload image to product
        :param product_id:
        :param image
        :return:
        """
        # Get
        data = []
        get_image_url = self.api_url + u'v3/catalog/products/' + str(product_id) + u'/images'
        try:
            response = requests.get(get_image_url, headers=self.bigcommerce_header)
            if response.status_code == 200:
                content = json.loads(response.content)
                data = content[u'data']
            else:
                self.logger.warning("Error get image info : {0}".format(response.content))
        except requests.ConnectionError:
            self.logger.error('Get Image Request Error.')
        except KeyError:
            self.logger.error('Get Image Key Error.')
        return data

    def upload_image(self, product_id, image):
        """
        Upload image to product
        :param product_id:
        :param image
        :return:
        image = {
            u'is_thumbnail': True,
            u'image_url': image_url
        }
        """
        # Post
        data = None
        image = json.dumps(image)
        self.logger.warning("Image info : {0}".format(image))
        upload_url = self.api_url + u'v3/catalog/products/' + str(product_id) + u'/images'
        try:
            response = requests.post(upload_url, headers=self.bigcommerce_header, data=image)
            if response.status_code == 200:
                content = json.loads(response.content)
                data = content[u'data']
                self.logger.warning("Image info : {0}".format(data))
            else:
                self.logger.warning("Error upload image info : {0}".format(response.content))
        except requests.ConnectionError:
            self.logger.error('Upload Image Request Error.')
        except KeyError:
            self.logger.error('Upload Image Key Error.')
        return data

    def upload_item_image(self, item, product_id):
        reg_exp = u'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'
        try:
            assets = item[u'assets']
            if assets:
                for asset in assets:
                    if asset[u'large_url']:
                        image_url_lg = asset[u'large_url']
                        image_lg = {
                            u'is_thumbnail': True,
                            u'image_url': image_url_lg
                        }
                        self.upload_image(product_id, image_lg)
                    elif asset[u'url']:
                        urls = re.findall(reg_exp, asset[u'url'])
                        if len(urls) > 0:
                            image_url = asset[u'url']
                            image = {
                                u'is_thumbnail': True,
                                u'image_url': image_url
                            }
                            self.upload_image(product_id, image)
        except KeyError:
            self.logger.warning("Error upload image info : {0}".format(item[u'id']))
