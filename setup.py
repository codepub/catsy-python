from distutils.core import setup

setup(
    name='catsy-python',
    version='1.0',
    packages=[
        'app',
        'app.catsy',
        'app.catsyWoocommerce',
        'app.catsyBigcommerce',
        'examples'
    ],
    package_data={'examples': ['data.json']},
    include_package_data=True,
    url='',
    license='',
    author='Catsy',
    author_email='',
    description=''
)
