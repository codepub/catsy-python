import logging
import sys
import csv

from app.catsy import Catsy
from examples import config

logger = logging.getLogger()


def setup_logger():
    logger.setLevel(logging.DEBUG)
    # Formatter
    FORMAT = "%(asctime)s [%(levelname)s] - %(message)s"
    DATE_FMT = '%m-%d %H:%M:%S'
    logging.basicConfig(format=FORMAT,
                        datefmt=DATE_FMT,
                        level=logging.DEBUG,
                        stream=sys.stderr)
    logger.debug('==========================================')
    logger.debug("== Script Started. Logging Configured.  ==")
    logger.debug("== Export EAV csv File ===================")
    logger.debug('==========================================')


def run():
    setup_logger()
    catsy = Catsy(config.TOKEN)
    limit = 100
    offset = 0

    region = 'US'
    export_attributes = [
        'marketing_long_description',
        'marketing_short_description',
        'marketing_bullet_1',
        'marketing_bullet_2',
        'marketing_bullet_3',
        'marketing_bullet_4',
        'marketing_bullet_5',
        'marketing_bullet_6',
        'marketing_bullet_7',
        'marketing_bullet_8',
        'marketing_bullet_9',
        'marketing_bullet_10',
        'marketing_bullet_11',
        'marketing_bullet_12',
        'elastic_features'
    ]
    multi_valued_attributes = [
        'elastic_features'
    ]

    attribute_map = catsy.attribute().get_attribute_map()
    items_count = catsy.query().items_count(config.QUERY_ID)

    query = catsy.query().get(config.QUERY_ID)
    filters = query['filters'] if 'filters' in query else None

    if filters is not None:
        filename = 'elastic_export_{}.csv'.format(region)
        with open(filename, 'w', newline='') as file:
            writer = csv.writer(file, delimiter='|')
            field = ["Region", "ProductNumber", "AttributeName", "AttributeSort", "AttributeText", "AttributeValueSort"]
            writer.writerow(field)

            while offset < items_count:
                items = catsy.item().get_items(filters, export_attributes, limit, offset)

                for item in items:
                    attribute_sort = 1
                    for key in export_attributes:
                        attribute_value_sort = 1
                        attribute = attribute_map[key] if key in attribute_map else None

                        if attribute is not None:
                            item_number = item['number'] if 'number' in item else ''
                            attribute_name = attribute['name'] if 'name' in attribute else ''
                            attribute_text = item[key] if key in item else ''

                            if attribute_text is not None and key in multi_valued_attributes:
                                values = attribute_text.split('|')
                                for value in values:
                                    if value:
                                        row = [region, item_number, attribute_name, attribute_sort, value, attribute_value_sort]
                                        writer.writerow(row)
                                        attribute_value_sort += 1
                            else:
                                row = [region, item_number, attribute_name, attribute_sort, attribute_text, attribute_value_sort]
                                writer.writerow(row)
                            attribute_sort += 1

                offset += limit
    else:
        raise Exception('filters are not valid in query')


if __name__ == '__main__':
    run()
