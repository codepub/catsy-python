import logging
import sys
from datetime import date, timedelta

from app.catsy import Catsy
from examples import config

logger = logging.getLogger()


def setup_logger():
    logger.setLevel(logging.DEBUG)
    # Formatter
    FORMAT = "%(asctime)s [%(levelname)s] - %(message)s"
    DATE_FMT = '%m-%d %H:%M:%S'
    logging.basicConfig(format=FORMAT,
                        datefmt=DATE_FMT,
                        level=logging.DEBUG,
                        stream=sys.stderr)
    logger.debug('==========================================')
    logger.debug("== Script Started. Logging Configured.  ==")
    logger.debug("== Get updated items from collection =====")
    logger.debug('==========================================')


def run():
    setup_logger()
    catsy = Catsy(config.TOKEN)
    # Get collection items only updated since yesterday
    yesterday = (date.today() - timedelta(1)).strftime('%Y-%m-%dT%H:%M:%SZ')
    tomorrow = (date.today() + timedelta(1)).strftime('%Y-%m-%dT%H:%M:%SZ')
    items = catsy.collection().items(config.COLLECTION_ID, 10, 0, 'UPDATED', yesterday, tomorrow)
    print(items)

if __name__ == '__main__':
    run()
