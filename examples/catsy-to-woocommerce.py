import logging
import sys

from app.catsyWoocommerce import WooCommerce

from app.catsy import Catsy
from examples import config

logger = logging.getLogger()


def setup_logger():
    logger.setLevel(logging.DEBUG)
    # Formatter
    FORMAT = "%(asctime)s [%(levelname)s] - %(message)s"
    DATE_FMT = '%m-%d %H:%M:%S'
    logging.basicConfig(format=FORMAT,
                        datefmt=DATE_FMT,
                        level=logging.DEBUG,
                        stream=sys.stderr)
    logger.debug('==========================================')
    logger.debug("== Script Started. Logging Configured.  ==")
    logger.debug("== Sync Items to Woocommerce =============")
    logger.debug('==========================================')


def run():
    setup_logger()
    catsy = Catsy(config.TOKEN)
    integration = {
        'baseUrl': config.WOOCOMMERCE_URL,
        'accessToken': config.WOOCOMMERCE_CONSUMER_KEY,
        'refreshToken': config.WOOCOMMERCE_CONSUMER_SECRET
    }

    woocommerce = WooCommerce(integration)

    # Get 10 items for example
    items = catsy.item().items(limit=10, offset=0)

    if items is None:
        return

    for item in items:
        try:
            woo_product = {
                'sku': item['number'],
                'name': item['name'],
                'description': item['description']
            }
            images = woocommerce.parse_item_images(item)
            woo_product['images'] = images
            result = woocommerce.search_product_by_sku(item['number'])
            if result:
                woocommerce.update_product(result['id'], woo_product)
            else:
                woocommerce.create_product(woo_product)
        except KeyError:
            logging.error('Missing required product info')
            pass

if __name__ == '__main__':
    run()
