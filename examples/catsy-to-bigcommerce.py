import logging
import sys

from app.catsyBigcommerce import Bigcommerce
from app.catsyBigcommerce import Product

from app.catsy import Catsy
from examples import config

logger = logging.getLogger()


def setup_logger():
    logger.setLevel(logging.DEBUG)
    # Formatter
    FORMAT = "%(asctime)s [%(levelname)s] - %(message)s"
    DATE_FMT = '%m-%d %H:%M:%S'
    logging.basicConfig(format=FORMAT,
                        datefmt=DATE_FMT,
                        level=logging.DEBUG,
                        stream=sys.stderr)
    logger.debug('==========================================')
    logger.debug("== Script Started. Logging Configured.  ==")
    logger.debug("== Sync Items to Woocommerce =============")
    logger.debug('==========================================')


def run():
    setup_logger()
    catsy = Catsy(config.TOKEN)

    bigcommerce = Bigcommerce(config.CLIENT_ID, config.CLIENT_TOKEN, config.STORE_URL)

    # Get 10 items for example
    items = catsy.item().items(limit=10, offset=0)

    if items is None:
        return

    for item in items:
        try:
            product = Product()
            product.set_sku(item['number'])
            product.set_name(item['name'])

            if 'description' in item:
                product.set_description(item['description'])

            # search product first
            results = bigcommerce.get_product(product.get_sku())
            if results and len(results) > 0:
                product_id = results[0][u'id']
                product_sku = results[0][u'sku']
                product_img = bigcommerce.get_image(product_id)
                if product_sku.lower() == product.get_sku().lower():
                    bigcommerce.update_product(product_id, product)
                    if len(product_img) <= 0:
                        # If the product does not have image, then upload
                        bigcommerce.upload_item_image(item, product_id)
            else:
                created_product = bigcommerce.create_product(product)
                if created_product and created_product.has_key(u'id'):
                    product_id = created_product[u'id']
                    # Upload images
                    bigcommerce.upload_item_image(item, product_id)
                else:
                    logger.info("Error create product : {0}".format(product.get_sku()))
        except KeyError:
            logging.error('Missing required product info')
            pass

if __name__ == '__main__':
    run()
