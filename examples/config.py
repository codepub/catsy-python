# please replace the below catsy api token
TOKEN = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'

QUERY_ID = 'e4835aff-a3e7-442b-85d7-8dadf895e668'

# Key is product attribute key, value is catsy attribute key
ATTRIBUTES_DICT = {
    'number': 'number',
    'name': 'name',
    'weight': 'weight',
    'product_adminlink': 'description',
    'product_line': 'title'
}

ITEMID_GET = 10647310

ITEMID_DELETE = 10647310

ITEMID_UPDATE = 10647310
UPDATE_ITEM_ATTRIBUTE_KEY = 'name'
UPDATE_ITEM_ATTRIBUTE_VALUE = 'Demo with Joe 1'


# WooCommerce Config
WOOCOMMERCE_URL = 'http://www.catalogforce.com'
WOOCOMMERCE_CONSUMER_KEY = 'ck_93ab13300e99475524941a33181ebba5804a0668'
WOOCOMMERCE_CONSUMER_SECRET = 'cs_46a22ab55c6872395f6adc25a6a54b986a421d94'


# Attribute Object
ATTRIBUTE = {
    'dataType': 'STRING',
    'editLines': 1,
    'editor': 'Default',
    'isLocalized': False,
    'name': 'Test 1',
    'key': 'test_1',
    'localeCode': 'en-US',
    'validationLevel': 'REQUIRED',
    'validationVersion': 'XSD',
    'validateXsd': '<?xml version="1.0" encoding="ISO-8859-1"?>↵<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema">↵    <xsd:element name="Root">↵        <xsd:complexType>↵            <xsd:sequence>↵                <xsd:element name="delete">↵                    <xsd:annotation>↵                        <xsd:documentation>String attribute.</xsd:documentation>↵                        <xsd:appinfo>↵                            <xsd:requiredLevel value="Optional"/>↵                            <xsd:displayName>Delete</xsd:displayName>   ↵                        </xsd:appinfo>↵                    </xsd:annotation>↵                    <xsd:simpleType>↵                        <xsd:restriction base="xsd:string">↵                            <xsd:maxLength value="4000"/>↵                            <xsd:minLength value="1"/>↵                        </xsd:restriction>↵                    </xsd:simpleType>↵                </xsd:element>↵            </xsd:sequence>↵        </xsd:complexType>↵    </xsd:element>↵</xsd:schema>'
}

ATTRIBUTE_GROUP_IDS = '20019664'
ROLE_ACCESS = [
    {"roleId": 4000005002, "roleAccessLevel": 2},
    {"roleId": 4000005003, "roleAccessLevel": 1},
    {"roleId": 4000005001, "roleAccessLevel": 2},
    {"roleId": 4000005004, "roleAccessLevel": 1}
]

# Asset path
ASSET_PATH = '/Users/admin/Documents/catsy/catsy-python/example/Small-mario.png'


# Collection
COLLECTION = {
    'name': 'Test',
    'skus_or_ids': [10819946, 10819947],
    'type': 'DIGITAL'
}

COLLECTION_ID = 4004643306

ROLE_ACCESS_IDS = '4000005002,4000005003'

# Bigcommerce
CLIENT_ID = 'ltpid71mypskc2viyqbv81q8o82oh2q'
CLIENT_TOKEN = 'lpm5p55lqscsfuld8op8x9n9puw58eg'
STORE_URL = 'https://api.bigcommerce.com/stores/ddetlr3vca/'

#
CHANNEL_ID = '20be923d-64de-4739-9465-9f0561b9d14f'
ITEM_SKUS = ['20001620']

ITEM_ATTRIBUTE_GROUP_IDS = ['20008915', '20025144']
