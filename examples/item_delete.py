import logging
import sys

from app.catsy import Catsy
from examples import config

logger = logging.getLogger()


def setup_logger():
    logger.setLevel(logging.DEBUG)
    # Formatter
    FORMAT = "%(asctime)s [%(levelname)s] - %(message)s"
    DATE_FMT = '%m-%d %H:%M:%S'
    logging.basicConfig(format=FORMAT,
                        datefmt=DATE_FMT,
                        level=logging.DEBUG,
                        stream=sys.stderr)
    logger.debug('==========================================')
    logger.debug("== Script Started. Logging Configured.  ==")
    logger.debug("== Sync Items to Catsy ===================")
    logger.debug('==========================================')


def run():
    setup_logger()
    # Delete an Item Example
    catsy = Catsy(config.TOKEN)
    catsy.item().delete(config.ITEMID_DELETE)

if __name__ == '__main__':
    run()