import logging
import sys

from app.catsy import Catsy
from examples import config

logger = logging.getLogger()


def setup_logger():
    logger.setLevel(logging.DEBUG)
    # Formatter
    FORMAT = "%(asctime)s [%(levelname)s] - %(message)s"
    DATE_FMT = '%m-%d %H:%M:%S'
    logging.basicConfig(format=FORMAT,
                        datefmt=DATE_FMT,
                        level=logging.DEBUG,
                        stream=sys.stderr)
    logger.debug('==========================================')
    logger.debug("== Script Started. Logging Configured.  ==")
    logger.debug("== Get Attributes ========================")
    logger.debug('==========================================')


def run():
    setup_logger()
    catsy = Catsy(config.TOKEN)
    channel = catsy.channel().get(config.CHANNEL_ID)
    if channel:
        if channel[u'dataSource'] == 'COLLECTION':
            catsy.collection().removeItem(channel[u'dataSourceId'], config.ITEM_SKUS)

if __name__ == '__main__':
    run()
