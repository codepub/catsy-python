import logging
import sys

from app.catsy import Catsy
from examples import config

logger = logging.getLogger()


def setup_logger():
    logger.setLevel(logging.DEBUG)
    # Formatter
    FORMAT = "%(asctime)s [%(levelname)s] - %(message)s"
    DATE_FMT = '%m-%d %H:%M:%S'
    logging.basicConfig(format=FORMAT,
                        datefmt=DATE_FMT,
                        level=logging.DEBUG,
                        stream=sys.stderr)
    logger.debug('==========================================')
    logger.debug("== Script Started. Logging Configured.  ==")
    logger.debug("== Get Items Updated Last Week ===========")
    logger.debug('==========================================')


def run():
    setup_logger()
    catsy = Catsy(config.TOKEN)
    filters = [{
        'attribute': {
            'key': 'update_date',
            'locale': 'global'
        },
        'operator': 'is',
        'value': 'last week'
    }]

    items = catsy.item().get_items(filters, 10, 0)
    print(items)

if __name__ == '__main__':
    run()
