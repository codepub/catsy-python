# README  
  
These instructions will get you a copy of the project up and running on your local machine.  
  
# What is this repository for?  
  
* Creates three sample items  
* Version 1.0  
* Catsy  
  
# How do I get set up?  
  
### INSTALL PYTHON, GIT, AND PIP  
* Install Python 2.7:  
    CentOS: https://www.digitalocean.com/community/tutorials/how-to-set-up-python-2-7-6-and-3-3-3-on-centos-6-4  
    Mac OS X / Windows: https://www.python.org/downloads/  
* Install git: https://gist.github.com/derhuerst/1b15ff4652a867391f03  
* Install pip: https://pip.pypa.io/en/stable/installation/  
  
### MAKE VIRTUALENV  
```
    git clone git@bitbucket.org:codepub/catsy-python.git  
    sudo pip install virtualenv  
    virtualenv catsy-python  
```
  
### INSTALL OTHER DEPENDENCIES  
```
    cd catsy-python  
    source bin/activate  
    pip install -r requirements.txt --ignore-installed (If failed, update pip version first: curl https://bootstrap.pypa.io/get-pip.py | python)
```

###  CONFIG API KEY
* Open examples/config.py file with your text editor and replace client TOKEN (API KEY)


# How do I run the tests?

### RUN "GET ITEMS" UPDATED IN A TIME WINDOW EXAMPLE (UPDATED LAST WEEK)
```
    rm -rf build
    python setup.py install
    python examples/items_get.py
```

### RUN "GET ASSET LIST" EXAMPLE
```
    rm -rf build
    python setup.py install
    python examples/assets_get.py
```

### RUN "GET ATTRIBUTES LIST" EXAMPLE
```
    rm -rf build
    python setup.py install
    python examples/attributes_get.py
```

### RUN "GET COLLECTION ITEMS LIST" EXAMPLE
* Open examples/config.py file with your text editor and replace COLLECTION_ID with the item id of which item you want to grab
```
    rm -rf build
    python setup.py install
    python examples/items_in_collection_get.py
```

### RUN "CREATE ITEM" EXAMPLE
* Open examples/config.py file with your text editor and replace ITEM_ATTRIBUTE_GROUP_IDS you want to add to current item
```
    python setup.py install
    python examples/item_post.py
```
* At this point, three sample items have been created in Catsy: CA522, Z26P154CTKIT, T20P254CLEEB

### RUN "UPDATE ITEM" EXAMPLE
* Open examples/config.py file with your text editor and replace ITEMID_UPDATE with the item id of which item you want to update
* Setup UPDATE_ITEM_ATTRIBUTE_KEY and UPDATE_ITEM_ATTRIBUTE_VALUE
```
    rm -rf build
    python setup.py install
    python examples/item_update.py
```

### RUN "DELETE ITEM" EXAMPLE
* Open examples/config.py file with your text editor and replace ITEMID_DELETE with the item id of which item you want to delete
```
    rm -rf build
    python setup.py install
    python examples/item_delete.py
```

### RUN "GET ITEM" EXAMPLE
* Open examples/config.py file with your text editor and replace ITEMID_GET with the item id of which item you want to grab
```
    python setup.py install
    python examples/item_get.py
```
* At this point, three sample items have been created in Catsy: CA522, Z26P154CTKIT, T20P254CLEEB

### RUN "SYNC ITEMS TO WOOCOMMERCE" EXAMPLE
* Open examples/config.py file with your text editor
* Replace WOOCOMMERCE_URL
* WOOCOMMERCE_CONSUMER_KEY
* WOOCOMMERCE_CONSUMER_SECRET
```
    rm -rf build
    python setup.py install
    python examples/catsy-to-woocommerce-simple.py
```

### RUN "CREATE ATTRIBUTE" EXAMPLE
* Open examples/config.py file with your text editor and replace ATTRIBUTE, ATTRIBUTE_GROUP_IDS, ROLE_ACCESS with the item id of which attribute you want to create
```
    rm -rf build
    python setup.py install
    python examples/attribute_post.py
```

### RUN "UPLOAD ASSET" EXAMPLE
* Open examples/config.py file with your text editor and replace ASSET_PATH with the asset file local path of which asset you want to upload
```
    rm -rf build
    python setup.py install
    python examples/asset_upload.py
```

### RUN "CREATE COLLECTION" EXAMPLE
* Open examples/config.py file with your text editor and replace COLLECTION, ROLE_ACCESS_IDS
```
    rm -rf build
    python setup.py install
    python examples/collection_post.py
```

### RUN "BIGCOMMERCE INTEGRATION" EXAMPLE
* Open examples/config.py file with your text editor and replace CLIENT_ID, CLIENT_TOKEN, STORE_URL
```
    rm -rf build
    python setup.py install
    python examples/catsy-to-bigcommerce.py
```

### RUN "ADD ITEM TO CHANNEL" EXAMPLE
* Open examples/config.py file with your text editor and replace CHANNEL_ID, ITEM_SKUS
```
    rm -rf build
    python setup.py install
    python examples/channel_item_add.py
```

### RUN "REMOVE ITEM FROM CHANNEL" EXAMPLE
* Open examples/config.py file with your text editor and replace CHANNEL_ID, ITEM_SKUS
```
    rm -rf build
    python setup.py install
    python examples/channel_item_remove.py
```

### RUN "GET ITEMS FROM CHANNEL" EXAMPLE
* Open examples/config.py file with your text editor and replace CHANNEL_ID
```
    rm -rf build
    python setup.py install
    python examples/items_in_channel_get.py
```

### RUN "EXPORT EAV CVS FILE" EXAMPLE
* Open examples/config.py file with your text editor and replace QUERY_ID
```
    rm -rf build
    python setup.py install
    python examples/eav_csv_export.py
```
  
# Who do I talk to?  
  
* support@catsy.com  
